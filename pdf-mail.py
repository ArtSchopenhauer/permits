from flask import Flask, render_template, request
import json
import requests
from flask_weasyprint import HTML, render_pdf
from flask_mail import Mail, Message

app = Flask(__name__)

app.config.update(
	DEBUG = True,
	MAIL_SERVER = 'smtp.gmail.com',
	MAIL_PORT = 465,
	MAIL_USE_SSL = True,
	MAIL_USE_TLS = False,
	MAIL_USERNAME = 'devzack5@gmail.com',
	MAIL_PASSWORD = 'red165red165'
	)

mail = Mail(app)

towns = {"Town of Hempstead": [{"item": "1", "name": "Triplicate Building Application", "count": "1"},
							  {"item": "2", "name": "Fast Track Application", "count": "1"},
							  {"item": "3", "name": "Workmans Compensation Insurance", "count": "1"},
							  {"item": "4", "name": "Certificate of Compliance", "count": "1"},
							  {"item": "5", "name": "Land Survey", "count": "3"},
							  {"item": "6", "name": "Construction Plans", "count": "2"},
							  {"item": "7", "name": "Manufacturer Data Sheets", "count": "2"}],
		 "Town of Babylon": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Property Owners Endoresement", "count": "1"},
							  {"item": "3", "name": "Solar Fast Track Checklist", "count": "1"},
							  {"item": "4", "name": "Solar Fast Track Permit Application", "count": "1"},
							  {"item": "5", "name": "Statement of Estimated Cost", "count": "1"},
							  {"item": "6", "name": "License & Insurance Pack", "count": "1"},
							  {"item": "7", "name": "Land Survey", "count": "3"},
							  {"item": "8", "name": "Construction Pack", "count": "2"}],
		 "Town of Islip": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Land Survey", "count": "3"},
							  {"item": "3", "name": "Construction Plans", "count": "3"},
							  {"item": "4", "name": "Manufacturer Data Sheets", "count": "2"},
							  {"item": "5", "name": "Certificate of Insurance", "count": "1"}],
		 "Town of Brookhaven": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Solar Permit Application", "count": "1"},
							  {"item": "3", "name": "Transactional Disclosure Form", "count": "1"},
							  {"item": "4", "name": "Certificate of Occupancy", "count": "1"},
							  {"item": "5", "name": "Land Survey", "count": "4"},
							  {"item": "6", "name": "Certificate of Insurance", "count": "1"},
							  {"item": "7", "name": "Construction Plans", "count": "3"}],
		 "Town of Smithtown": [{"item": "1", "name": "Solar Fast Track Permit Application", "count": "2"},
							  {"item": "2", "name": "Construction Pack", "count": "3"}, 
							  {"item": "3", "name": "Insurance Pack", "count": "1"}],
		 "Town of Huntington": [{"item": "1", "name": "Solar Fast Track Permit Application", "count": "1"},
							  {"item": "2", "name": "Building Permit Application", "count": "2"},
							  {"item": "3", "name": "Construction Plans", "count": "4"},
							  {"item": "4", "name": "Insurance Pack", "count": "1"},
							  {"item": "5", "name": "Tax Bill", "count": "1"}],
		 "Village of Lindenhurst": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "8 Property Photos", "count": "1"},
							  {"item": "3", "name": "Land Survey", "count": "4"},
							  {"item": "4", "name": "Construction Plans", "count": "4"},
							  {"item": "5", "name": "Statement of Estimated Cost", "count": "1"},
							  {"item": "6", "name": "Certificate of Insurance", "count": "1"}],
		 "Town of Southold": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Certificate of Occupancy", "count": "1"},
							  {"item": "3", "name": "1st Page of Storm Water Application", "count": "1"},
							  {"item": "4", "name": "Electrical Inspection Form", "count": "1"},
							  {"item": "5", "name": "Land Survey", "count": "1"},
							  {"item": "6", "name": "Insurance Pack", "count": "1"}],
		 "Village of Mastic Beach": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Transactional Disclosure Form", "count": "1"},
							  {"item": "3", "name": "Certificate of Structures Affidavit", "count": "1"},
							  {"item": "4", "name": "Land Survey", "count": "2"},
							  {"item": "5", "name": "Tax Bill", "count": "1"},
							  {"item": "6", "name": "Certificate of Occupancy", "count": "1"},
							  {"item": "7", "name": "Insurance Pack", "count": "1"},
							  {"item": "8", "name": "Contractor's License", "count": "1"},
							  {"item": "9", "name": "Statement of Estimated Cost", "count": "1"},
							  {"item": "10", "name": "Construction Pack", "count": "2"}],
		 "Village of Babylon": [{"item": "1", "name": "Solar Fast Track Permit Application", "count": "1"},
							  {"item": "2", "name": "Engineering Calculations", "count": "1"},
							  {"item": "3", "name": "Statement of Estimated Cost", "count": "1"},
							  {"item": "4", "name": "Manufacturer Data Sheets", "count": "1"},
							  {"item": "5", "name": "Home Improvement License", "count": "1"},
							  {"item": "6", "name": "Electrical License", "count": "1"},
							  {"item": "7", "name": "Insurance Pack", "count": "1"},
							  {"item": "8", "name": "Construction Pack", "count": "4"}],
		 "VIllage of Port Jefferson": [{"item": "1", "name": "Building Permit Application", "count": "4"},
							  {"item": "2", "name": "Solar Permit Application", "count": "4"},
							  {"item": "3", "name": "Short Environmental Assessment Form", "count": "4"},
							  {"item": "4", "name": "Disclosure Affidavit", "count": "4"},
							  {"item": "5", "name": "Land Survey", "count": "4"},
							  {"item": "6", "name": "Tax Bill", "count": "1"},
							  {"item": "7", "name": "Certificate of Occupancy", "count": "1"},	
							  {"item": "8", "name": "Insurance Pack", "count": "1"},
							  {"item": "9", "name": "Contractor's License", "count": "1"},
							  {"item": "10", "name": "Statement of Estimated Cost", "count": "1"}],
		 "Town of Riverhead": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Disclosure Affidavit", "count": "1"},
							  {"item": "3", "name": "Electrical Application", "count": "1"},
							  {"item": "4", "name": "Fast Track Checklist", "count": "1"},
							  {"item": "5", "name": "Fast Track Permit Application Sheet", "count": "1"},
							  {"item": "6", "name": "Land Survey", "count": "1"},
							  {"item": "7", "name": "License & Insurance Pack", "count": "1"},
							  {"item": "8", "name": "Construction Pack", "count": "2"}],
		 "Village of Northport": [{"item": "1", "name": "Building Permit Application", "count": "2"},
							  {"item": "2", "name": "Land Survey", "count": "3"},
							  {"item": "3", "name": "Property Photos", "count": "1"},
							  {"item": "4", "name": "Insurance Pack", "count": "1"},
							  {"item": "5", "name": "Manufacturer Data Sheets", "count": "1"},
							  {"item": "6", "name": "Statement of Estimated Cost", "count": "1"}],
		 "Town of Shelter Island": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Land Survey", "count": "3"},
							  {"item": "3", "name": "License & Insurance Pack", "count": "3"},
							  {"item": "4", "name": "Home Improvement License", "count": "3"},
							  {"item": "5", "name": "Construction Plans", "count": "1"}],
		 "Town of East Hampton": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Land Survey", "count": "1"},
							  {"item": "3", "name": "License & Insurance Pack", "count": "1"},
							  {"item": "4", "name": "Home Improvement License", "count": "1"},
							  {"item": "5", "name": "Construction Plans", "count": "2"}],
		 "Town of Southampton": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Fast Track Checklist", "count": "1"},
							  {"item": "3", "name": "Fast Track Application Info Sheet", "count": "1"},
							  {"item": "4", "name": "License & Insurance Pack", "count": "1"},
							  {"item": "5", "name": "Home Improvement License", "count": "1"},
							  {"item": "6", "name": "Construction Plans", "count": "3"}],
		 "Village of Patchogue": [{"item": "1", "name": "Building Permit Application", "count": "3"},
							  {"item": "2", "name": "Liability Insurance", "count": "1"},
							  {"item": "3", "name": "Workmans Compensation Insurance", "count": "1"},
							  {"item": "4", "name": "Contractor's License", "count": "1"},
							  {"item": "5", "name": "Certificate of Occupancy", "count": "1"}],
		 "Village of Southampton": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Disclosure Affidavit", "count": "1"},
							  {"item": "3", "name": "Owners Consent Form", "count": "1"},
							  {"item": "4", "name": "Workmans Compensation Insurance", "count": "1"},
							  {"item": "5", "name": "Liability Insurance", "count": "1"},
							  {"item": "6", "name": "Contractor's License", "count": "1"}],
		 "Village of East Hampton": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Certificate of Occupancy Request", "count": "1"},
							  {"item": "3", "name": "Land Survey", "count": "1"},
							  {"item": "4", "name": "Workmans Compensation Insurance", "count": "1"},
							  {"item": "5", "name": "Liability Insurance", "count": "1"},
							  {"item": "6", "name": "Contractor's License", "count": "1"}],
		 "Town of Oyster Bay": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Homeowner Consent Form", "count": "1"},
							  {"item": "3", "name": "Insurance Pack", "count": "1"},
							  {"item": "4", "name": "Land Survey", "count": "2"},
							  {"item": "5", "name": "Construction Plans", "count": "2"},
							  {"item": "6", "name": "Manufacturer Data Sheets", "count": "2"}],
		 "Village of Brightwaters": [{"item": "1", "name": "Notarized Application", "count": "3"},
							  {"item": "2", "name": "Land Survey", "count": "3"},
							  {"item": "3", "name": "Contractor's License", "count": "1"},
							  {"item": "4", "name": "Insurance Pack", "count": "1"},
							  {"item": "5", "name": "House Photos", "count": "1"}],
		 "Village of Islandia": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Certificate of Occupancy", "count": "1"},
							  {"item": "3", "name": "Land Survey", "count": "1"},
							  {"item": "4", "name": "Insurance Pack", "count": "1"},
							  {"item": "5", "name": "Construction Plans", "count": "3"}],		
		 "Need Requirements For This Municipality!": [{"item": "1", "name": "Need Info Here", "count": "0"}],		
		 "Village of Lake Grove": [{"item": "1", "name": "Building Permit Application", "count": "1"},
							  {"item": "2", "name": "Insurance Pack", "count": "1"},
							  {"item": "3", "name": "Contractor's License", "count": "1"},
							  {"item": "4", "name": "Construction Plans", "count": "2"}]}			  


contacts = "https://levelsolar.secure.force.com/api/services/apexrest/contacts"
accounts = "https://levelsolar.secure.force.com/api/services/apexrest/accounts"
cases = "https://levelsolar.secure.force.com/api/services/apexrest/cases"

cost_per_kW = 4.879

# permanent values
client_id = "geugjjmpvsysqxx3kdxp7r77hika0yhm"
client_secret = "Cg31t7GJTtoe2tgdcZjLFZjTpsJKHrcq"
ny_customer_folder_id = "1763301834"

def get_token():
	token_file = open("/root/lboard/token.json", "r")
	token_json = json.load(token_file)
	refresh_token = token_json["refresh_token"]
	token_file.close()
	get_token_url = "https://app.box.com/api/oauth2/token"
	body = {"grant_type": "refresh_token",
			"refresh_token": refresh_token,
			"client_id": client_id,
			"client_secret": client_secret}
	response_object = requests.post(get_token_url, data=body)
	response_body = response_object.json()
	access_token = response_body["access_token"]
	file_object = open("/root/lboard/token.json", "w")
	json.dump(response_body, file_object)
	file_object.close()
	return access_token

def download_five_min(county, city, account_number):
	data = "File"
	access_token = get_token()
	headers = {"Authorization": "Bearer " + access_token}
	folder_items_url = "https://api.box.com/2.0/folders/" + ny_customer_folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if item["name"] == county:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if item["name"] == city:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if account_number in item["name"]:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if "Permits" in item["name"]:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if item["type"] == "file":
			if "5 min" in item["name"] or "5min" in item["name"]:
				file_id = item["id"]
				download_file_url = "https://api.box.com/2.0/files/" + file_id + "/content"
				response_object = requests.get(download_file_url, headers=headers)
				data = response_object.content
				return data
	if data == "File":
		return "Not Found"
	else:
		return data

def download_elec(county, city, account_number):
	data = "File"
	access_token = get_token()
	headers = {"Authorization": "Bearer " + access_token}
	folder_items_url = "https://api.box.com/2.0/folders/" + ny_customer_folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if item["name"] == county:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if item["name"] == city:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if account_number in item["name"]:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if "Permits" in item["name"]:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if "Fund Documents" in item["name"]:
			folder_id = item["id"]
			folder_items_url = "https://api.box.com/2.0/folders/" + folder_id + "/items"
	folder_items = requests.get(folder_items_url, headers=headers).json()
	for item in folder_items["entries"]:
		if item["type"] == "file":
			if "elec" in item["name"] or "Elec" in item["name"]:
				file_id = item["id"]
				download_file_url = "https://api.box.com/2.0/files/" + file_id + "/content"
				response_object = requests.get(download_file_url, headers=headers)
				data = response_object.content
	if data == "File":
		return "Not Found"
	else:
		return data

@app.route("/")
def submission():
	return render_template("submission.html")

@app.route("/pseg_phase_ii", methods=["GET", "POST"])
def submit():
	if request.method == "POST":
		sfdc = request.form["sfdc"]
		email = request.form["email"]
		account_param = {"account_number": sfdc}
		account_response_object = requests.get(accounts, params=account_param)
		account_list = json.loads(account_response_object.text)
		if len(account_list) == 0:
			error_customer = "Customer not found in Salesforce."
			suggestion_customer = "Please make sure the customer number is correct."
			return render_template("error.html", error=error_customer, suggestion=suggestion_customer)
		county = account_list[0]["municipality"]["county"]["name"]
		full_address = account_list[0]["name"]
		split_address = full_address.split(" - ")
		address, city, zip_code = split_address
		account_id = account_list[0]["id"]
		install_param = {"type_name": "install", "account": account_id}
		install_response_object = requests.get(cases, params=install_param)
		install_list = json.loads(install_response_object.text)
		lipa_case_param = {"type_name": "lipa case", "account": account_id}
		lipa_case_response_object = requests.get(cases, params=lipa_case_param)
		lipa_case_list = json.loads(lipa_case_response_object.text)
		if len(install_list) > 0:
			install = install_list[0]
			if install["total_system_size"]:
				total_system_size_dc = install["total_system_size"]
			else:
				total_system_size_dc = "Missing"
		else:
			total_system_size_dc = "Missing"
		if total_system_size_dc == "Missing":
			total_system_size_ac = "Missing"
		else:
			total_system_size_ac = str(((float(total_system_size_dc))*.9))
		if len(lipa_case_list) > 0:
			lipa_case = lipa_case_list[0]
			if lipa_case["psegphase1_approval_date"]:
				approval_date_iso = lipa_case["psegphase1_approval_date"]
				approval_date = approval_date_iso.split("T")[0]
			else:
				approval_date = "Missing"
			if lipa_case["pseg_pam_id"]:
				pam_id = lipa_case["pseg_pam_id"]
			else: 
				pam_id = "Missing"
		else:
			approval_date = "Missing"
			pam_id = "Missing"
		if total_system_size_dc == "Missing":
			error_system_size = "System Size not found in Salesforce."
			suggestion_system_size = "Please ensure this info is in the Install Case."
			return render_template("error.html", error=error_system_size, suggestion=suggestion_system_size)
		if approval_date == "Missing":
			error_approval_date = "PSEG Phase I Approval Date not found in Salesforce."
			suggestion_approval_date = "Please ensure this info is in the LIPA Case."
			return render_template("error.html", error=error_approval_date, suggestion=suggestion_approval_date)
		if pam_id == "Missing":
			error_pam_id = "PAM ID not found in Salesforce."
			suggestion_pam_id = "Please ensure this info is in the LIPA Case."
			return render_template("error.html", error=error_pam_id, suggestion=suggestion_pam_id)
		contact_param = {"account": account_id}
		contact_response_object = requests.get(contacts, params=contact_param)
		contact_list = json.loads(contact_response_object.text)
		len_list = len(contact_list)
		i = 0
		while i < len_list:
			if contact_list[i]["phone"] or contact_list[i]["mobilephone"]:
				contact = contact_list[i]
				i = i + 1
			else:
				i = i + 1
		contact_name = contact["name"]
		info ={"name": contact_name,
			   "address": address,
			   "city": city,
			   "zip_code": zip_code,
			   "total_system_size_dc": total_system_size_dc,
			   "total_system_size_ac": total_system_size_ac,
			   "approval_date": approval_date,
			   "pam_id": pam_id}
		html = render_template("app_i.html", info=info)
		msg = Message('PSEG Phase II for %s' %info["name"], sender='devzack5@gmail.com', recipients=[email])
		msg.body = "Appendix I, Electrical Certificate, and 5 Minute Test for %s are attached." % info["name"]
		pdf = HTML(string=html).write_pdf()
		msg.attach("Appendix I - %s.pdf" %info["name"], "application/pdf", pdf)
		five_min_pdf = download_five_min(county, city, sfdc)
		if five_min_pdf == "Not Found":
			error_five_min = "No 5 Minute Test found in Box."
			suggestion_five_min = "Please ensure that the file is correctly named."
			return render_template("error.html", error=error_five_min, suggestion=suggestion_five_min)
		else:	
			msg.attach("5 Minute Test - %s.pdf" %info["name"], "application/pdf", five_min_pdf)
			electrical_pdf = download_elec(county, city, sfdc)
			if electrical_pdf == "Not Found":
				error_electrical = "No Electrical Certificate found in Box."
				suggestion_electrical = "Please ensure that the file is correctly named."
				return render_template("error.html", error=error_electrical, suggestion=suggestion_electrical)
			else:
				msg.attach("Electrical Certificate - %s.pdf" %info["name"], "application/pdf", electrical_pdf)
				mail.send(msg)
				return render_template("submit_again.html")
	else:
		return render_template("submission.html")

@app.route("/pdf", methods=["GET", "POST"])
def pdf():
	if request.method == "POST":
		sfdc = request.form["sfdc"]
		account_param = {"account_number": sfdc}
		account_response_object = requests.get(accounts, params=account_param)
		account_list = json.loads(account_response_object.text)
		full_address = account_list[0]["name"]
		split_address = full_address.split(" - ")
		address, city, zip_code = split_address
		account_id = account_list[0]["id"]
		document_param = {"type_name": "document", "account": account_id}
		document_response_object = requests.get(cases, params=document_param)
		document_list = json.loads(document_response_object.text)
		design_param = {"type_name": "design", "account": account_id}
		design_response_object = requests.get(cases, params=design_param)
		design_list = json.loads(design_response_object.text)
		if len(document_list) > 0:
			document = document_list[0]
			if document["tax_map_id"]:
				tax_map_id = document["tax_map_id"]
			else:
				tax_map_id = "Missing"
		else:
			tax_map_id = "Missing"
		if len(design_list) > 0:
			for item in design_list:
				if item["status"] == "Closed":
					design = item
					if design["number_of_panels"]:
						number_of_panels = design["number_of_panels"]
					else:
						number_of_panels = "Missing"
					if design["total_system_size"]:
						total_system_size = design["total_system_size"]
					else:
						total_system_size = "Missing"
				else:
					number_of_panels = "Missing"
					total_system_size = "Missing"
		else:
			number_of_panels = "Missing"
			total_system_size = "Missing"
		if number_of_panels == "Missing" or total_system_size == "Missing":
			module_power = "Missing"
			total_system_cost = "Missing"
		else:
			module_power = "{:.0f}".format(((float(total_system_size))*1000)/(int(number_of_panels)))
			total_system_cost = "{:,.0f}".format((float(total_system_size))*1000*(cost_per_kW))
		contact_param = {"account": account_id}
		contact_response_object = requests.get(contacts, params=contact_param)
		contact_list = json.loads(contact_response_object.text)
		len_list = len(contact_list)
		i = 0
		while i < len_list:
			if contact_list[i]["phone"] or contact_list[i]["mobilephone"]:
				contact = contact_list[i]
				i = i + 1
			else:
				i = i + 1
		if contact["phone"]:
			phone = contact["phone"]
		elif contact["mobilephone"]:
			phone = contact["mobilephone"]
		if contact["account"]["municipality"]:
			if contact["account"]["municipality"]["name"] in towns:
				municipality = contact["account"]["municipality"]["name"]
			else:
				municipality = "Need Requirements For This Municipality!"
		info = {"name": contact["name"],
				"street_address": address,
				"city": city,
				"state": contact["state"],
				"zip": zip_code,
				"phone": phone,
				"municipality": municipality,
				"tax_map_id": tax_map_id,
				"number_of_panels": number_of_panels,
				"total_system_size": total_system_size,
				"module_power": module_power,
				"cost_per_kW": str(cost_per_kW),
				"total_system_cost": total_system_cost}
		town_info = towns[info["municipality"]]
		html = render_template("pdf.html", info=info, town_info=town_info)
		return render_pdf(HTML(string=html))
	else:
		return render_template("submission.html")

if __name__ == "__main__":
	app.run(debug=True)
